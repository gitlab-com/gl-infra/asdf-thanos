# Contributing

Testing Locally:

```shell
asdf plugin test <plugin-name> <plugin-url> [--asdf-tool-version <version>] [--asdf-plugin-gitref <git-ref>] [test-command*]

#
asdf plugin test thanos https://gitlab.com/gitlab-com/gl-infra/asdf-thanos.git "thanos --version"
```

Tests are automatically run in GitHub Actions on push and PR.
